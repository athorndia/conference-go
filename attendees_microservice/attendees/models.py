from django.db import models
from django.urls import reverse


class ConferenceVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    name = models.CharField(max_length=200)


class Attendee(models.Model):
    """
    The Attendee model represents someone that wants to attend
    a conference.

    ONE CONFERENCE HAS MANY ATTENDEES!
    """

    email = models.EmailField()
    name = models.CharField(max_length=200)
    company_name = models.CharField(max_length=200, null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)

    conference = models.ForeignKey(
        ConferenceVO,
        related_name="attendees",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name

    def create_badge(self):
        """
        This method checks if the attendee instance already has a badge
        associated with it through its badge property (created by the
        OneToOneField). If it does not, it creates a new Badge instance with
        the attendee instance as the value for the attendee property of the
        Badge. The hasattr built-in function is used to check if the badge
        property exists on the attendee instance.
        Create a badge for this attendee if one does not already exist.
        """
        if not hasattr(self, 'badge'):
            badge = Badge(attendee=self)
            badge.save()

        # def create_badge(self):
        #     try:
        #         self.badge
        #     except ObjectDoesNotExist:
        #         Badge.objects.create(attendee=self)

    def get_api_url(self):
        return reverse("api_show_attendee", kwargs={"id": self.id})


class Badge(models.Model):
    """
    The Badge model represents the badge an attendee gets to
    wear at the conference.

    Badge is a Value Object and, therefore, does not have a
    direct URL to view it.

    ONE BADGE FOR ONE ATTENDEE!
    """

    created = models.DateTimeField(auto_now_add=True)

    attendee = models.OneToOneField(
        Attendee,
        related_name="badge",
        on_delete=models.CASCADE,
        primary_key=True,
    )


'''
Testing out Model Methods:
python manage.py shell

from attendees.models import Attendee
attendee = Attendee.objects.get(id=1)
attendee.create_badge()
print(attendee.badge)
attendee.refresh_from_db()
attendee.create_badge()
print(attendee.badge)

'''
