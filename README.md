# Code Go!

<p align="center">
    <img src="https://media0.giphy.com/media/bYg33GbNbNIVzSrr84/giphy.gif?cid=ecf05e47gufp9zbpk5f74dwdluktxpz4p4qhqjdgrr98fjjt&ep=v1_gifs_search&rid=giphy.gif&ct=g" width= "300">
</p>

## Conference Management Application

This project is an API-based application that allows software developers to plan and attend conferences. With this software, users can propose different presentations and have them reviewed for approval or rejection. The application also allows vendors and sponsors to set up booths and showcase their products.

---

## Key Features

Create conferences with physical locations
Sign up for conferences
Attendees receive badges with QR codes to connect with other attendees
Propose presentations and have them reviewed for approval or rejection
Vendors and sponsors can set up booths and showcase their products

---

## Technology Stack

- API client: Insomnia
- DIY JSON Library
- RESTful API URLs

---

## 3rd-party data

- Pexels Search for Photos API to get the URL of a picture for the city
- OpenWeather: Geocoding API to translate the city and state for the conference into latitude and longitude
- Current weather data API to get the current weather for the latitude and longitude
- Docker containers to transform the project into having a microservice architecture

---

## Target Audience

This application is designed for software developers who want to attend conferences to learn about specific technologies, meet and network with other developers, and enjoy good food and drinks.

---

## Implementation Details

The project uses aggregate refactoring to identify bounded contexts. It also implements a RESTful API architecture and utilizes third-party data to enhance the user experience. The application is built inside Docker containers to transform it into a microservice architecture.

---

## Resources / Reference Materials

[dj-database-url](https://pypi.org/project/dj-database-url/)
[Setting PostgreSQL connection values at the Django documentation pages](https://docs.djangoproject.com/en/4.0/ref/databases/#postgresql-connection-settings-1)
[The postgres base image on Docker Hub, which you can configure with environment variables](https://hub.docker.com/_/postgres)
[Setting environment variables in Docker Compose files](https://docs.docker.com/compose/compose-file/compose-file-v3/#environment)
