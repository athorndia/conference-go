from django.http import JsonResponse
from .models import Conference, Location, State
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json
from .acls import get_photo, get_weather_data


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = ["name", "picture_url"]


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "picture_url",
    ]

    def get_extra_data(self, o):
        return {"state": o.state.abbreviation}


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = ["name"]


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]
    encoders = {
        "location": LocationListEncoder(),
    }
# ###############################################################


@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceListEncoder,
        )
    # POST
    else:
        content = json.loads(request.body)

        # Get the Location object and put it in the content dict
        try:
            location = Location.objects.get(id=content["location"])
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
    conference = Conference.objects.create(**content)
    return JsonResponse(
        conference,
        encoder=ConferenceDetailEncoder,
        safe=False,
    )

# ###############################################################


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_conference(request, id):
    if request.method == "GET":
        conference = Conference.objects.get(id=id)
        weather = get_weather_data(
            conference.location.city, conference.location.state.abbreviation
            )
        return JsonResponse(
           {"conference": conference, "weather": weather},
           encoder=ConferenceDetailEncoder,
           safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Conference.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})

    elif request.method == "PUT":
        content = json.loads(request.body)

        # Get the Conference object and put it in the content dict
        try:
            ...
        except Conference.DoesNotExist:
            ...

        Conference.objects.filter(id=id).update(**content)
        conference = Conference.objects.get(id=id)

        # Define weather before using it
        weather = get_weather_data(
            conference.location.city, conference.location.state.abbreviation
            )

        return JsonResponse(
            {
                "conference": conference,
                "weather": weather
            },
            encoder=ConferenceDetailEncoder,
            safe=False,
        )

# ###############################################################


@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    # * create an if block for the code to run only if the HTTP method is GET
    if request.method == "GET":
        locations = Location.objects.all()
        return JsonResponse(
            {"locations": locations},
            encoder=LocationListEncoder
        )
    else:
        # handle POST request here.
        content = json.loads(request.body)
        try:
            # Get the State object and put it in the content dict
            # so that we can pass it to Location.objects.create
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state

        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )

        # Get the state's full name
        state_full_name = state.name
        print(state_full_name)

        # Get photo URL from Pexels API
        picture_url = get_photo(f'{content["city"]} {state_full_name}')
        content["picture_url"] = picture_url

        location = Location.objects.create(**content)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )


# the content of the POST is a JSON-formatted string stored in request.body
# we need to decode that using the json module
# use json.loads to convert the JSON-formatted string in request.body,
# then use that with Location.objects.create.

# ###############################################################

@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_location(request, id):
    if request.method == "GET":
        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        try:
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
                state_full_name = state.name
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )

        # Define location before using it
        location = Location.objects.get(id=id)

        # Get new picture URL if city or state changed
        if content["city"] != location.city or content["state"] != location.state:
            picture_url = get_photo(
                f'{content["city"]} {state_full_name}'
                )
            content["picture_url"] = picture_url

        Location.objects.filter(id=id).update(**content)

        location = Location.objects.get(id=id)
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
