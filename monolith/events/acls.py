from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests


def get_photo(query):
    """
    Get the URL of a picture from the Pexels API
    """

    # Create a dictionary for the headers to use in the request
    headers = {
        "Authorization": f"{PEXELS_API_KEY}",
    }

    # Create the URL for the request with the city and state (aka query)
    url = f"https://api.pexels.com/v1/search?query={query}"

    # Make the request
    response = requests.get(url, headers=headers)
    # Parse the JSON response
    api_dict = response.json()
    # Return a dictionary that contains a 'picture_url' key and
    # one of the URLs for one of the pictures in the response.
    return {'picture_url': api_dict['photos'][0]['src']['original']}


def get_weather_data(city, state):
    """
    Create the URL for the Geocoding API with the city and state
    """

    url = "http://api.openweathermap.org/geo/1.0/direct"

    params = {
        "q": f"{city},{state},US",
        "limit": 1,
        "appid": OPEN_WEATHER_API_KEY,
    }

    # Make the request
    response = requests.get(url, params=params)
    # Parse the JSON response
    content = response.json()
    # Get the latitude and longitude from the response
    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except (KeyError, IndexError):
        return None

    params = {
        "appid": OPEN_WEATHER_API_KEY,
        "lat": latitude,
        "lon": longitude,
        "units": "imperial",
    }

    # Create the URL for the Weather API with the latitude and longitude
    url = "https://api.openweathermap.org/data/2.5/weather"

    # Make the request
    response = requests.get(url, params=params)
    # Parse the JSON response
    content = response.json()

    # Get the main temperature and the weather's description
    # and put them in a dictionary
    # Return the dictionary
    return {
        "temperature": content["main"]["temp"],
        "description": content["weather"][0]["description"],
    }


'''
This will:

Build the geocoding API URL with the city and state
Make the geocoding API request
Get the latitude and longitude from the response
Build the weather API URL with the latitude and longitude
Make the weather API request
Get the temperature and description from the response
Return a dictionary with the temperature and description
'''
