from django.http import JsonResponse
from common.json import ModelEncoder
import json
from django.views.decorators.http import require_http_methods
from .models import Presentation
from events.models import Conference
from events.api_views import ConferenceListEncoder


# 404 Error: server understood request but unable to
# fulfill requested resource.
# 400 Error: server did not understand/unable to process due
# to bad syntax or other client-side errors.
# 500 Error: server encountered an unexpected condition


# A class to encode the status of a presentation.
# class PresentationStatusEncoder(ModelEncoder):
#     model = Status
#     properties = ["name"]

# ###############################################################


# A class to encode a list of presentations.
class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = ["title"]

# ###############################################################


# A class to encode the details of a specified presentation.
class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceListEncoder(),
    }

    def get_extra_data(self, o):
        return {"status": o.status.name}

# ###############################################################

# 'GET' - returns a list of presentations for the specified conference
# 'POST' - creates a new presentation for the specified conference and
# returns its data


@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    if request.method == "GET":
        presentations = Presentation.objects.filter(
            conference=conference_id
        )
        return JsonResponse(
            {"presentations": presentations},
            encoder=PresentationListEncoder,
        )
    elif request.method == "POST":
        content = json.loads(request.body)

        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

        presentation = Presentation.create(**content)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )


# ###############################################################

# 'GET' - returns the details of the specified presentation
# 'DELETE' - deletes the specified presentation and returns a deleted message
# 'PUT' - updates the specified presentation and returns its details


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_presentation(request, id):
    if request.method == "GET":
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Presentation.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        if "status" in content:
            if content["status"] == "APPROVED":
                Presentation.objects.get(id=id).approve()
                del content["status"]
            elif content["status"] == "REJECTED":
                Presentation.objects.get(id=id).reject()
                del content["status"]
            # elif content["status"] == "PENDING":
            #     Presentation.objects.get(id=id).pend()
            #     del content["status"]
            else:
                return JsonResponse(
                    {"message": "Invalid status entry"},
                    status=400,
                    safe=False,
                )
        try:
            if "conference" in content:
                conference = Conference.objects.get(
                    id=content["conference"]
                )
                content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )
        Presentation.objects.filter(id=id).update(**content)
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )


"""
Lists the presentation titles and the link to the
presentation for the specified conference id.

This one to get a list of presentations for a conference.

Returns a dictionary with a single key "presentations"
which is a list of presentation titles and URLS. Each
entry in the list is a dictionary that contains the
title of the presentation, the name of its status, and
the link to the presentation's information.

{
    "presentations": [
        {
            "title": presentation's title,
            "status": presentation's status name
            "href": URL to the presentation,
        },
        ...
    ]
}
presentations = [
    {
        "title": p.title,
        "status": p.status.name,
        "href": p.get_api_url(),
    }
    for p in Presentation.objects.filter(conference=conference_id)

]
return JsonResponse({"presentations": presentations})
REVIEWING A LIST VIEW:
That one uses a list comprehension rather than a for loop to
populate the list of presentations. Those are then used
in the dictionary as the value for the "presentations"
key of the dictionary passed to the JsonResponse.
"""

"""
Returns the details for the Presentation model specified
by the id parameter.

This once to get the details for a presentation.

This should return a dictionary with the presenter's name,
their company name, the presenter's email, the title of
the presentation, the synopsis of the presentation, when
the presentation record was created, its status name, and
a dictionary that has the conference name and its URL

{
    "presenter_name": the name of the presenter,
    "company_name": the name of the presenter's company,
    "presenter_email": the email address of the presenter,
    "title": the title of the presentation,
    "synopsis": the synopsis for the presentation,
    "created": the date/time when the record was created,
    "status": the name of the status for the presentation,
    "conference": {
        "name": the name of the conference,
        "href": the URL to the conference,
    }
}
    return JsonResponse({
    "presenter_name": presentation.presenter_name,
    "company_name": presentation.company_name,
    "presenter_email": presentation.presenter_email,
    "title": presentation.title,
    "synopsis": presentation.synopsis,
    "created": presentation.created,
    "status": presentation.status.name,
    "conference": {
        "name": presentation.conference.name,
        "href": presentation.conference.get_api_url(),
    }
}
)
"""
